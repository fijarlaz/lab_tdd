from django import forms
from .models import Status

class StatusForm(forms.Form):
    attrs = {
        'class' : 'form-control',
        'id' : 'formstatus',
    }
    status = forms.CharField( max_length=300,required=True, widget=forms.TextInput(attrs=attrs))