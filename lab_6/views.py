from django.shortcuts import render
from .forms import StatusForm
from .models import Status
# Create your views here.

landing_page_content = 'Hello yow apa kabs'

response={}
def index(request):
    the_status = StatusForm(request.POST or None)
    response['statusform'] = the_status
    stats = Status.objects.all()
    response['all_status'] = stats
    html = 'index.html'
    if (request.method == "POST"):
        if (the_status.is_valid()):
            temp = request.POST['status']
            stat = Status(status=temp)
            stat.save()
            response['status'] = stats
            return render(request, html,response)
        else:
            return render(request, html, response)
    else:
        
        return render(request, html, response)


def profile(request):
    return render(request, 'profile.html')