from django.test import TestCase, Client
from django.test import LiveServerTestCase
from django.urls import resolve
from .models import Status
from .views import index, landing_page_content, profile
from .forms import StatusForm
from selenium.webdriver.chrome.options import Options
from django.http import HttpRequest
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
# Create your tests here.

class Lab6UnitTest(TestCase):

    def test_lab_6_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_status(self):
        #make a new status
        new_status = Status.objects.create(status = 'Saya sedang bahagia')

        #retrieve status
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)



    # def contain_hello(self):
    #     response = Client.get('')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('<p>Hello yow apa kabs</p>', html_response)



    def landing_page_complete(self):
        response = Client.get('')
        html_response = response.content.decode('utf8')
        self.assertIn(landing_page_content, html_response)

    def test_landing_page_not_null(self):
        self.assertIsNotNone(landing_page_content)

    # def status_less_or_equal_than_three_hundreds(self):
    #     temp =Status.objects.create(status='sjefoisehoisheofsiheofsihefoisdsdsdsdsdsdsdsshefoishefoishefoishefoishefoihseofihseofihseoifhsoeifhsoiefhsoiefhsoihfsoiefhsoiehfosiefhsoiefhsoiehfsoiehfosiehfosiehfosiehfosiehflskdfosiehlskdfosielskdfosiehflskdfhosiehlskdhfosiehlskhfosiehlskdhfosiehflskdfhosiehflskdhflsiehflskdhfsiehflsefhsliehflskdhfsliehflskfsli')
    #     self.assertFalse(len(temp.status)==300)

    def test_lab_6_using_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')



class Lab6_Test_Profile(TestCase):
    def test_lab_6_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_lab_6_profile_using_right_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')


class statusUpdate(unittest.TestCase):


    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(statusUpdate,self).setUp()
        


    def test_update_status(self):
        
        self.selenium.get('http://localhost:8000/')
        assert 'Hello' in self.selenium.title
        status = self.selenium.find_element_by_id('formstatus')
        status.send_keys('Coba coba')
        status.submit()
        assert 'Coba coba' in self.selenium.page_source
        assert 'Tell me what you feel!' in self.selenium.page_source
        
        
        
        
        time.sleep(3)

    # def test_css(self):
        # body_background = self.selenium.find_element_by_tag_name('body').value_of_css_property('background')
        # table_position = self.selenium.find_element_by_tag_name('table').value_of_css_property('text-align')
        # table_container = self.selenium.find_element_by_class_name('table-container').value_of_css_property('background-color')
        # self.assertIn('white', table_container)
        # self.assertIn('center', table_position)
        # self.assertIn('linear-gradient(black, pink)', body_background)
        # time.sleep(2)

    def tearDown(self):
        
        self.selenium.quit()


if __name__ == '__main__':
    unittest.main(warnings='ignore')